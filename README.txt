onWebChat

onWebChat module for Drupal
by Theodoros Vasiloudis
====================================

DESCRIPTION
-----------
onWebChat live chat module for Drupal provides an easy way to integrate your
Drupal website with our innovative live chat system


INSTALLATION
------------
1. Create an account on [onwebchat](https://www.onwebchat.com).

2. Place this module in your modules directory
(drupal_home_folder/sites/all/modules) and enable it.

3. Copy the "Chat Id" from onWebChat
[operator console](https://www.onwebchat.com/login.php) or registration email.

4. Visit admin / settings / web services / onWebChat on your Drupal site
and paste the "Chat Id" into the textfield. Click the 'Save configuration'
button.
